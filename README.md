# OpenML dataset: fertility

https://www.openml.org/d/1473

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: David Gil, Jose Luis Girela
   
**Source**: UCI

**Please cite**: David Gil, Jose Luis Girela, Joaquin De Juan, M. Jose Gomez-Torres, and Magnus Johnsson. Predicting seminal quality with artificial intelligence methods. Expert Systems with Applications, 39(16):12564 - 12573, 2012  

Source:

David Gil, 
dgil '@' dtic.ua.es, 
Lucentia Research Group, Department of Computer Technology, University of Alicante 

Jose Luis Girela, 
girela '@' ua.es, 
Department of Biotechnology, University of Alicante

Attribute Information:

Season in which the analysis was performed. 1) winter, 2) spring, 3) Summer, 4) fall. (-1, -0.33, 0.33, 1) 
Age at the time of analysis. 18-36 (0, 1) 
Childish diseases (ie , chicken pox, measles, mumps, polio) 1) yes, 2) no. (0, 1) 
Accident or serious trauma 1) yes, 2) no. (0, 1) 
Surgical intervention 1) yes, 2) no. (0, 1) 
High fevers in the last year 1) less than three months ago, 2) more than three months ago, 3) no. (-1, 0, 1) 
Frequency of alcohol consumption 1) several times a day, 2) every day, 3) several times a week, 4) once a week, 5) hardly ever or never (0, 1) 
Smoking habit 1) never, 2) occasional 3) daily. (-1, 0, 1) 
Number of hours spent sitting per day ene-16 (0, 1) 
Output: Diagnosis normal (N), altered (O)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1473) of an [OpenML dataset](https://www.openml.org/d/1473). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1473/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1473/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1473/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

